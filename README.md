## malml

A toy lisp like interpreter written in OCaml.

## About

This is a simple AST walker interpreter for a toy lisp like language.
It is inspired in spirit by [MAL](https://github.com/kanaka/mal) though I did not actually read the code or strictly followed the steps.

It does not aim at being useable, and was initially a attempt at learning Rust. As you can see it is now written in OCaml, not in Rust as I rewrite
the begining of it in an attempt at proving myself that I was not incompetent, and that Rust was just a terrible language ! (just kidding... maybe).

Indeed, I wrote this working OCaml version in an afternoon, while I could not finish the lexer in the same time in Rust (f*ck you borrow checker).

I am pretty happy with the design of this one, as it is fairly simple and let me play with some Monadic style programming.

I may or may not go further in the development, my interest being in implementing a basic macro system.
