exception Not_reached

let (-|) f g =  (* compose operator *)
  fun param -> param |> f |> g

let ( >>= ) a f =  (* bind operator for Result monad *)
  match a with
  | Ok thing -> f thing
  | Error err -> Error err

(* binding let for the reader *)
let ( let* ) = ( >>= )

(* binding let for the evaluator *)
let ( >>=! ) a f = match a with
  | (Ok expr, _) -> f expr
  | (Error msg, env) -> (Error msg, env)

let ( let*! ) = ( >>=! )

(* ========================================= *
 *                  EXPR                     *
 * ========================================= *)
type atom_t =
  | Number of float
  | String of string
  | Symbol of string

type primitive_t =
  | If
  | Add
  | Mul
  | Sub
  | Div
  | Def
  | Do
  | Lambda

type expr_t =
  | NotAnExpr
  | Atom of atom_t
  | List of (expr_t list)
  | Primitive of primitive_t
  | Function of (string List.t * expr_t)

let make_number_atom text =
  match Float.of_string_opt text with
  | Some v -> Ok (Atom (Number v))
  | None -> Error "Cannot reparse a number"

let make_symbol_atom text = Atom (Symbol text)

let make_string_atom text = Atom (String text)

let join sep = function
  | [] -> ""
  | single :: [] -> single
  | first :: rest ->
    List.fold_left (fun acc elem -> acc ^ sep ^ elem) first rest

let rec format_expr = function
  | Atom (Number v) -> Float.to_string v
  | Atom (String v) -> "\"" ^ v ^ "\""
  | Atom (Symbol v) -> v
  | NotAnExpr -> ""
  | List elems ->
    "(" ^ (join " " (List.map format_expr elems)) ^ ")"
  | Primitive If -> "#prim<if>"
  | Primitive Add -> "#prim<+>"
  | Primitive Mul -> "#prim<*>"
  | Primitive Sub -> "#prim<->"
  | Primitive Div -> "#prim</>"
  | Primitive Def -> "#prim<def>"
  | Primitive Do -> "#prim<do>"
  | Primitive Lambda -> "#prim<Lambda>"
  | Function (params, _) -> "#fun<" ^ (join " " params) ^ ">"


let is_number = function
  | Atom (Number _) -> true
  | _ -> false

let is_string = function
  | Atom (String _) -> true
  | _ -> false

let is_list = function
  | List _ -> true
  | _ -> false

let is_symbol = function
  | Atom (Symbol _) -> true
  | _ -> false

let print_expr = format_expr -| print_endline

(* ========================================= *
 *                 READER                    *
 * ========================================= *)

type token_t =
  | OPar
  | CPar
  | StringAtom of string
  | NumberAtom of string
  | SymbolAtom of string


let list_to_string lst =
  let buf = Buffer.create (List.length lst) in
  List.iter (Buffer.add_char buf) lst;
  Buffer.contents buf

let tokenize line =
  let rec tokenize_number stream acc =
    match Seq.uncons stream with
    | Some ('0' as c, rest) | Some ('1' as c, rest)
    | Some ('2' as c, rest) | Some ('3' as c, rest)
    | Some ('4' as c, rest) | Some ('5' as c, rest)
    | Some ('6' as c, rest) | Some ('7' as c, rest)
    | Some ('8' as c, rest) | Some ('9' as c, rest)
    | Some ('.' as c, rest) ->
      tokenize_number rest (c :: acc)
    | _ -> Some (NumberAtom (list_to_string (List.rev acc)), stream)
  in
  let rec tokenize_string stream acc =
    match Seq.uncons stream with
    | Some ('"', rest) ->
      Some (StringAtom (list_to_string (List.rev acc)), rest)
    | Some (c, rest) ->
      tokenize_string rest (c :: acc)
    | None -> None (* "unexpected EOF in string." *)
  in
  let rec tokenize_symbol stream acc =
    match Seq.uncons stream with
    | Some ('(', _) | Some (')', _)
    | Some (' ', _) | Some ('\r', _)
    | Some ('\t', _) | Some ('\n', _)
    | Some ('"', _)
    | None ->
      Some (SymbolAtom (list_to_string (List.rev acc)), stream)
    | Some (c, rest) ->
      tokenize_symbol rest (c :: acc)
  in
  let rec tokenize_helper stream =
    match Seq.uncons stream with
    | Some ('(', rest) -> Some (OPar, rest)
    | Some (')', rest) -> Some (CPar, rest)
    | Some ('0', _) | Some ('1', _)
    | Some ('2', _) | Some ('3', _)
    | Some ('4', _) | Some ('5', _)
    | Some ('6', _) | Some ('7', _)
    | Some ('8', _) | Some ('9', _) ->
      tokenize_number stream []
    | Some ('"', rest) ->
      tokenize_string rest []
    | Some (' ', rest) | Some ('\t', rest)
    | Some ('\n', rest) | Some ('\r', rest) ->
      tokenize_helper rest
    | Some _ -> tokenize_symbol stream []
    | None -> None
  in
  Seq.unfold
    tokenize_helper
    (Seq.init (String.length line) (String.get line))

let print_token = function
  | OPar -> print_endline "OPar"
  | CPar -> print_endline "CPar"
  | StringAtom s -> print_endline ("sa!" ^ s)
  | NumberAtom s -> print_endline ("na!" ^ s)
  | SymbolAtom s -> print_endline ("Sa!" ^ s)

let print_tokens tokens =
  Seq.iter print_token tokens; tokens

let parse tokens =
  let rec parse_list tokens acc =
    match Seq.uncons tokens with
    | Some (CPar, rest) -> Ok (List (List.rev acc), rest)
    | _ ->
      parse_helper tokens >>= function
      | (NotAnExpr, _) -> Error "Unexpected EOF in list."
      | (expr, rest) -> parse_list rest (expr :: acc)
  and parse_helper tokens =
    match Seq.uncons tokens with
    | Some (OPar, rest) -> parse_list rest []
    | Some (CPar, _) -> Error "Unexpected closing paren."
    | Some (StringAtom s, rest) -> Ok (make_string_atom s, rest)
    | Some (SymbolAtom s, rest) -> Ok (make_symbol_atom s, rest)
    | Some (NumberAtom s, rest) ->
      let* at = make_number_atom s in Ok (at, rest)
    | None -> Ok (NotAnExpr, tokens)
  in
  let* (expr, _) = parse_helper tokens in Ok expr

let read = tokenize
           (* -| print_tokens *)
           -| parse

(* ========================================= *
 *                  EVAL                     *
 * ========================================= *)

let primitives =
  Hashtbl.of_seq
    ( List.to_seq
        [ "+", Add ;
          "def", Def ;
          "/", Div ;
          "do", Do ;
          "if", If ;
          "\\", Lambda ;
          "lambda", Lambda ;
          "*", Mul ;
          "-", Sub ; ] )

let yes = (Atom (Symbol "true"))
let no = (Atom (Symbol "false"))
let undefined = (Atom (Symbol "undefined"))

let init_env () =
  let h = Hashtbl.create 8 in 
  Hashtbl.add h "true" yes;
  Hashtbl.add h "false" no;
  Hashtbl.add h "undefined" undefined;
  h :: []


let rec from_env env s =
  match env with
  | first :: rest ->
    ( match Hashtbl.find_opt first s with
      | Some thing -> Ok thing
      | None -> from_env rest s )
  | [] ->
    match Hashtbl.find_opt primitives s with
    | Some p -> Ok (Primitive p)
    | None ->  Error ("Unbound symbol " ^ s ^ ".")

let add_frame env = (Hashtbl.create 4) :: env

let set_env env s value =
  match env with
  | first :: _ -> Hashtbl.add first s value
  | [] -> raise Not_reached

let eval env =
  (* Evaluate any expression *)
  let rec eval_helper env = function
    | NotAnExpr -> (Error "Found a non expression (bug).", env)
    | Atom (Number n) -> (Ok (Atom (Number n)), env)
    | Atom (String s) -> (Ok (Atom (String s)), env)
    | Atom (Symbol s) -> (from_env env s, env)
    | List (first :: rest) ->
      let*! callee = (eval_helper env first) in
      let*! ret = eval_call env callee rest in
      (Ok ret, env)
    | List [] -> (Ok (List []), env)
    | Primitive name -> (Ok (Primitive name), env)
    | Function def -> (Ok (Function def), env)

  and eval_param_list env params after =
    let rec eval_params acc = function
      | first :: rest ->
        let*! param = eval_helper env first in
        eval_params (param :: acc) rest
      | [] -> (Ok (List (List.rev acc)), env)
    in
    eval_params [] params >>=!
    ( function
      | List params -> after params
      | _ -> raise Not_reached )

  (* Evaluate all call form *)
  and eval_call env callee params =
    match callee with
    | Primitive name -> eval_primitive_call env params name
    | Function (args, body) ->
      eval_param_list env params
        (fun params -> eval_user_call env args params body)
    | _ -> (Error "Non callable called", env)

  (* Evaluate primitive invocations *)
  and eval_primitive_call env params = function
    | Add -> math_fold env ( +. ) 0. params
    | Sub -> math_fold_but_one env ( -. ) 0. params
    | Mul -> math_fold env ( *. ) 1. params
    | Div -> math_fold_but_one env ( /. ) 1. params

    | Def ->
      ( match params with
        | Atom (Symbol name) :: expr :: [] ->
          let*! value = eval_helper env expr in
          set_env env name value; (Ok yes, env)
        | List ((Atom (Symbol name)) :: params) :: expr :: [] ->
          let*! func = make_function params expr in
          set_env env name func; (Ok yes, env)
        | _ -> (Error "Ill formed def.", env) )

    | Do ->
      let new_env = add_frame env in
      let rec loop acc = function
        | expr :: rest ->
          let*! value = eval_helper new_env expr in
          loop value rest
        | [] -> (Ok acc, env)
      in
      loop undefined params

    | If ->
      ( match params with
        | test :: iftrue :: iffalse :: [] ->
          let*! cond = eval_helper env test in
          if cond == no then
            let*! res = eval_helper env iffalse in (Ok res, env)
          else
            let*! res = eval_helper env iftrue in (Ok res, env)
        | test :: iftrue :: [] ->
          let*! cond = eval_helper env test in
          if cond == no then
            (Ok no, env)
          else
            let*! res = eval_helper env iftrue in (Ok res, env)
        | _ -> (Error "Ill formed if condition.", env) )
    | Lambda ->
      ( match params with
        | (List params) :: expr :: [] ->
          make_function params expr
        | _ -> (Error "Ill formed lambda.", env) )

  (* Evaluate user defined functions call *)
  and eval_user_call env args params body :
    ((expr_t, string) Result.t * (string, expr_t) Hashtbl.t List.t) =
    let new_env = add_frame env in
    List.iter2 (set_env new_env) args params;
    let*! retval = eval_helper new_env body in
    (Ok retval, env)

  and math_fold env op neutral params =
    eval_param_list env params
      ( fun params -> 
          let apply acc = function
            | Atom (Number v) -> op acc v
            | _ -> raise Not_reached
          in
          if List.for_all is_number params then
            ( Ok (Atom (Number (List.fold_left apply neutral params))), env)
          else ( Error "Non numeric parameter.", env) )

  and math_fold_but_one env op neutral = function
    | [] -> (Error "This operator applies to at least one parameter.", env)
    | first :: [] ->
      eval_helper env first >>=!
      ( function
        | Atom (Number n) -> (Ok (Atom (Number (op neutral n))), env)
        | _ -> (Error "Non numeric parameter.", env) )
    | first :: rest ->
      eval_helper env first >>=!
      ( function
        | Atom (Number f) -> math_fold env op f rest
        | _ -> (Error "Non numeric parameter.", env) )

  and make_function params body =
    if List.for_all is_symbol params then
      let extract_name = function
        | Atom (Symbol name) -> name
        | _ -> raise Not_reached
      in
      let params_names = List.map extract_name params in
      (Ok (Function (params_names, body)), env)
    else (Error "Ill formed lambda (non symbol parameter).", env)

  in
  function 
  | Ok NotAnExpr -> (Ok NotAnExpr, env)
  | Ok expr -> eval_helper env expr
  | Error msg -> (Error msg, env)

(* ========================================= *
 *                  MAIN                     *
 * ========================================= *)

let print = function
  | (Ok expr, env) -> print_expr expr; env
  | (Error msg, env) -> print_endline msg; env

let repl () =
  let rec loop env =
    try
      ( print_string "> "; read_line () ) |> read |> (eval env) |> print |> loop
    with
    | End_of_file -> print_endline "Exiting."
  in
  loop (init_env ())

let () = repl ()
